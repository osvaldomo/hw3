#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <ctime>
#include <iostream>
#include "get_time.h"
#define n 1024
long long A[n][n];
long long B[n][n];
long long C[n][n];


int main() {

  int s = 5;
  
  for(int i = 0; i < n; ++i)
  {
      for(int j = 0; j < n; ++j)
      {      
        A[i][j] = i+j;
        B[i][j] = i+(2*j);
        C[i][j] = 0;
      }
  }

  timer t;
cilk_for(int i1 = 0; i1 < n; i1+=s)
  cilk_for(int j1 = 0; j1 < n; j1+=s)
    for(int k1 = 0; k1 < n; k1+=s)
      for(int i = i1; i < i1+s && i< n; i++)
        for(int j = j1; j < j1+s && j<n; ++j)
          for(int k = k1; k < k1+s && k<n; ++k)
            C[i][j] += A[i][k] * B[k][j];

  t.stop();
  std::cout << "Value of C[10][20]: " << C[10][20] << std::endl;
  std::cout << "elapsed time: " << t.get_total() << std::endl;

  return 0;
}
