#include <ctime>
#include <iostream>
#include "get_time.h"
#define n 1024
long long A[n][n];
long long B[n][n];
long long C[n][n];


int main() {

  for(int i = 0; i < n; ++i)
  {
      for(int j = 0; j < n; ++j)
      {      
        A[i][j] = i+j;
        B[i][j] = i+(2*j);
        C[i][j] = 0;
      }
  }

  
  timer t;

  for(int i = 0; i < n; ++i)
  {
      for(int j = 0; j < n; ++j)
      {
          for(int k = 0; k < n; ++k)
          {
            C[i][j] += A[i][k] * B[k][j];
          }
      }
  }

  t.stop();
  std::cout << "Value of C[10][20]: " << C[10][20] << std::endl;
  std::cout << "elapsed time: " << t.get_total() << std::endl;

  return 0;
}