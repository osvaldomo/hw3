#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <ctime>
#include <iostream>
#include "get_time.h"

void Rec_Mult(long *C, long *A, long *B, int n, int rowsize)
{
  if(n == 1)
  {
    C[0] += A[0] * B[0];
  }
  else
  {
    int d11 = 0;
    int d12 = n/2;
    int d21 = (n/2) * rowsize;
    int d22 = (n/2) * (rowsize + 1);

    cilk_spawn Rec_Mult(C+d11, A+d11, B+d11, n/2, rowsize);
    cilk_spawn Rec_Mult(C+d21, A+d22, B+d21, n/2, rowsize);
    cilk_spawn Rec_Mult(C+d12, A+d11, B+d12, n/2, rowsize);
    Rec_Mult(C+d22, A+d22, B+d22, n/2, rowsize);
    cilk_sync;
    cilk_spawn Rec_Mult(C+d11, A+d12, B+d21, n/2, rowsize);
    cilk_spawn Rec_Mult(C+d21, A+d21, B+d11, n/2, rowsize);
    cilk_spawn Rec_Mult(C+d12, A+d12, B+d22, n/2, rowsize);
    Rec_Mult(C+d22, A+d21, B+d12, n/2, rowsize);
    cilk_sync;

  }

}


int main() {

  int n = 1024;
  long *A = new long[n*n];
  long *B = new long[n*n];
  long *C = new long[n*n];
  for(int i = 0; i < n; ++i)
  {
    for(int j = 0; j < n; ++j)
    {
      A[i*n+j] = i+j;
      B[i*n+j] = i+(2*j);
      C[i*n+j] = 0;
    }
    
  }

  timer t;
  std::cout << "dead 1" <<std::endl;
  Rec_Mult(C, A, B, n, n);
  std::cout << "dead 2" <<std::endl;
  t.stop();

  std::cout << "Value of C[10][20]: " << C[10*n+20] << std::endl;
  std::cout << "elapsed time: " << t.get_total() << std::endl;

  delete[] A;
  delete[] B;
  delete[] C;
  std::cout << "dead end" <<std::endl;
  return 0;
}